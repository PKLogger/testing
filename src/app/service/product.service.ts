import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  URL = 'http://localhost:3000/product';
  // constructor(private httpClient: HttpClient) { }
  httpClient = inject(HttpClient);

  getListAllProducts(): Observable<Product[]>{
    return this.httpClient.get<Product[]>(this.URL);
  }

  saveProduct(product: Product): Observable<Product[]>{
    return this.httpClient.post<Product[]>(this.URL, product);
  }
}

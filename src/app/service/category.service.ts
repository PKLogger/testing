import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from '../model/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  URL = 'http://localhost:3000/category';
  // constructor(private httpClient: HttpClient) { }
  httpClient = inject(HttpClient);

  getListAllCategories(): Observable<Category[]>{
    return this.httpClient.get<Category[]>(this.URL);
  }
}

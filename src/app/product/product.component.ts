import { Component, OnInit } from '@angular/core';
import { NgFor } from '@angular/common';
import { Product } from '../model/product';
import { ProductService } from '../service/product.service';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-product',
  standalone: true,
  imports: [NgFor, RouterLink],
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent implements OnInit {
  products: Product[] = [];
  constructor(private productService: ProductService) {
    
  }
  ngOnInit(): void {
    this.fetchData();
  }

  fetchData() {
   this.productService.getListAllProducts().subscribe({
    next: (res) => this.products = res
   })
  }
}

import { Component, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { Category } from '../model/category';
import { CategoryService } from '../service/category.service';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ProductService } from '../service/product.service';
import { Product } from '../model/product';

@Component({
  selector: 'app-create-product',
  standalone: true,
  imports: [RouterLink, ReactiveFormsModule],
  templateUrl: './create-product.component.html',
  styleUrl: './create-product.component.css'
})
export class CreateProductComponent implements OnInit {
  categories: Category[] = [];

  productCreateForm = new FormGroup({
    name:  new FormControl("", Validators.required),
    price:  new FormControl(""),
    category:  new FormControl("")
  })
  constructor(private categoryService: CategoryService, private productService: ProductService, private router: Router) {

  }

  ngOnInit(): void {
    this.fetchDataCategory();
  }

  fetchDataCategory() {
    this.categoryService.getListAllCategories().subscribe({ next: (res) => this.categories = res })
  }

  submit() {
    const product = this.productCreateForm.value as Product;
    this.productService.saveProduct(product).subscribe({ next: () => this.router.navigateByUrl("") })
  }

  get fCOntrol() {
    return this.productCreateForm.controls;
  }
}

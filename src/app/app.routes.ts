import { Routes } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { CreateProductComponent } from './create-product/create-product.component';

export const routes: Routes = [
    {path: "", component: ProductComponent},
    {path: "create-product", component: CreateProductComponent}
];
